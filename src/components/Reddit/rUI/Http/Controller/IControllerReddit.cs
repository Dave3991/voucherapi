﻿namespace UI.Http.Controller;

/// <summary>
/// Interface for a controller, because we need to address all controllers in a generic way for assembly scanning in Setup class.
/// </summary>
public interface IControllerReddit
{
  
}
