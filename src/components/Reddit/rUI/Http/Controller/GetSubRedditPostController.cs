﻿using Application.CQRS.GetRedditPost;
using Infrastructure;
using Libs.Interface;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace UI.Http.Controller;

/// <summary>
/// Reddit Controller for Http
/// </summary>
[Route("api/reddit")]
[Produces("application/json")]
[ApiController]
public class GetSubRedditPostController: ControllerBase, IControllerReddit
{
    private readonly IMediator _mediator;
    
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="mediator"></param>
    public GetSubRedditPostController(IMediator mediator)
    {
      _mediator = mediator;
    }
    
    /// <summary>
    /// Get SubReddit Post
    /// </summary>
    ///
    [HttpGet("getSubRedditPost")]
    public  string GetSubRedditPosts()
    {
      var result = _mediator.Send(new GetRedditPostQuery("aww"));
      return "wdwad";
    }



}
