﻿using System.ComponentModel.Composition;
using Libs.Interface;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using UI.Http.Controller;

namespace UI.Setup;

// export so it can be discovered by MEF in ModuleConfigurator (see ModuleConfigurator.cs)
/// <inheritdoc />
[Export(typeof(IModule))]
public class Module: IModule
{
  /// <inheritdoc />
  public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
  {
    var assembly = typeof(IControllerReddit).Assembly;
    services.AddMvcCore().AddApplicationPart(assembly).AddControllersAsServices();
  }
}
