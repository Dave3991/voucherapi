﻿using Infrastructure;
using MediatR;

namespace Application.CQRS.GetRedditPost;

public class GetRedditPostQueryHandler: RequestHandler <GetRedditPostQuery, string>
{
  private readonly RedditLoader _redditLoader;

  public GetRedditPostQueryHandler(RedditLoader redditLoader)
  {
    _redditLoader = redditLoader;
  }

  protected override string Handle(GetRedditPostQuery request)
  {
    return "dwadf"; //_redditLoader.GetPosts(request.SubRedditName);
  }
}
