﻿using Infrastructure;
using MediatR;

namespace Application.CQRS.GetRedditPost;

public class GetRedditPostQuery: IRequest<string>
{
   public string SubRedditName { get; set; }
   
   public GetRedditPostQuery(string subRedditName)
   {
       SubRedditName = subRedditName;
   }
}
