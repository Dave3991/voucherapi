﻿using System.ComponentModel.Composition;
using Application.CQRS.GetRedditPost;
using Libs.Interface;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Application.Setup;

// export so it can be discovered by MEF in ModuleConfigurator (see ModuleConfigurator.cs)
[Export(typeof(IModule))]
public class Module: IModule
{
  public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
  {
    // register MediatR
    services.AddMediatR(AppDomain.CurrentDomain.GetAssemblies());
  }
}
