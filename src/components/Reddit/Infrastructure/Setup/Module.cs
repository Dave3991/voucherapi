﻿using System.ComponentModel.Composition;
using Libs.Interface;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.Setup;

// export so it can be discovered by MEF in ModuleConfigurator (see ModuleConfigurator.cs)
[Export(typeof(IModule))]
public class Module: IModule
{
  public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
  {
    services.AddTransient<RedditLoader>();
  }
}
