﻿using System.ComponentModel.Composition;
using System.Configuration;
using Application.Interface;
using Domain.Interfaces;
using Infrastructure.Postgres.Database;
using Infrastructure.Postgres.Repository;
using Infrastructure.Postgres.Repository.CreateVoucherRepository;
using Libs.Interface;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.Setup;

// export so it can be discovered by MEF in ModuleConfigurator (see ModuleConfigurator.cs)
[Export(typeof(IModule))]
public class Module: IModule
{
  public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
  {
    services.AddDbContext<VoucherDbContext>(c =>
      c.UseNpgsql(configuration.GetConnectionString("VoucherDbContext")));
    services.AddTransient<ICreateVoucherRepository, CreateVoucherRepository>();
    services.AddTransient<IGetVoucherRepository, GetVoucherRepository>();
  }
}
