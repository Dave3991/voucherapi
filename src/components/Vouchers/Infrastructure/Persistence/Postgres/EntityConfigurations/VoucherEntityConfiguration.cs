using Domain.Entities.Postgres;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EntityConfigurations;

public class VoucherEntityConfiguration : IEntityTypeConfiguration<VoucherEntity>
{
  public void Configure(EntityTypeBuilder<VoucherEntity> builder)
  {
    builder.ToTable("Vouchers");
    builder.HasKey(x => x.Id);
    builder.Property(x => x.Id).ValueGeneratedOnAdd();
    builder.Property(x => x.VoucherCode).IsRequired();
    builder.Property(x => x.Discount).IsRequired();
    builder.Property(x => x.ExpirationDate).IsRequired();
    builder.Property(x => x.IsActive).IsRequired();
  }
}
