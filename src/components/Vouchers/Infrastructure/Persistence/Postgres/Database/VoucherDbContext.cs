﻿using Domain.Entities.Postgres;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Infrastructure.Postgres.Database;

public class VoucherDbContext: DbContext
{
  private readonly ILoggerFactory _loggerFactory;

  public VoucherDbContext(DbContextOptions<VoucherDbContext> options, ILoggerFactory loggerFactor): base(options)
  {
    _loggerFactory = loggerFactor;
  }

  //setup database tables
  public DbSet<VoucherEntity> VouchersEntities => Set<VoucherEntity>();
  
}
