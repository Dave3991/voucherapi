﻿using Domain.Entities.Postgres;
using Domain.Events;
using Domain.Interfaces;
using Domain.ValueObjects;
using MediatR;

namespace Infrastructure.Postgres.Repository.CreateVoucherRepository;

public class CreateVoucherRepository: ICreateVoucherRepository
{
  private readonly IMediator _mediator;

  public CreateVoucherRepository(IMediator mediator)
  {
    _mediator = mediator;
  }

  public VoucherEntity CreateVoucher(CreateVoucherVo createVoucherVo)
  {
   
    var voucher = new VoucherEntity("afawfawf");
    _mediator.Publish(new VoucherCreatedEvent(voucher));
    return voucher;
  }
}
