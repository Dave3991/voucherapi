using Elastic.Clients.Elasticsearch;
using Elastic.Transport;
using Microsoft.Extensions.Configuration;

namespace Infrastructure.Persistence.Elastic
{
	public class ElasticConnector
	{
		private readonly ElasticsearchClient _client;

		public ElasticConnector(IConfiguration configuration)
		{
			var settings = new ElasticsearchClientSettings(new Uri(configuration.GetConnectionString("Elasticsearch")))
				.DefaultIndex("ainews-articles")
				.Authentication(new ApiKey("xxxx"));

			_client = new ElasticsearchClient(settings);
		}

		public ElasticsearchClient GetClient()
		{
			return _client;
		}
	}
}
