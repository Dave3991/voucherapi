﻿using Domain.Entities.Postgres;

namespace Application.Interface;

public interface IGetVoucherRepository
{
  public VoucherEntity GetVoucher(int voucherId);
}
