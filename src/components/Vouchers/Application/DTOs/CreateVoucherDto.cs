﻿namespace Application.Dto;

public class CreateVoucherDto
{
  public string RequestCurrency { get; }
  public decimal RequestAmount { get; }
  public string RequestClientId { get; }
  public CreateVoucherDto(string requestCurrency, decimal requestAmount, string requestClientId)
  {
    this.RequestCurrency = requestCurrency;
    this.RequestAmount = requestAmount;
    this.RequestClientId = requestClientId;
  }
}
