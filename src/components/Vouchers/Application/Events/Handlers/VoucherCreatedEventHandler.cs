using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Domain.Events;

namespace Application.Events.Handlers
{
  public class VoucherCreatedEventHandler : INotificationHandler<VoucherCreatedEvent>
  {
    public Task Handle(VoucherCreatedEvent notification, CancellationToken cancellationToken)
    {
      // Handle the event here
      // If we want to work with DOMAIN, we must use CQRS command/query to properly use MEDIATR behavior
      // set up in the application layer (application/Server/Behavior/Mediatr/LoggingBehaviorPipeline.cs)

      return Task.CompletedTask;
    }
  }
}
