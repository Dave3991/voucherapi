using Domain.Case;
using MediatR;

namespace Application.CQRS.Write.DeleteVoucher
{
  public class DeleteVoucherHandler: RequestHandler<DeleteVoucherCommand>
  {
  
    private readonly DeleteVoucherCase _deleteVoucherCase;

    public DeleteVoucherHandler(DeleteVoucherCase deleteVoucherCase)
    {
      _deleteVoucherCase = deleteVoucherCase;
    }

    protected override void Handle(DeleteVoucherCommand request)
    {
      _deleteVoucherCase.DeleteVoucher(request.VoucherId);
    }
  }
}
