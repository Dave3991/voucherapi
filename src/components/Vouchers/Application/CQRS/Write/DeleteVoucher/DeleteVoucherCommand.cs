using MediatR;

namespace Application.CQRS.Write.DeleteVoucher;

public class DeleteVoucherCommand: IRequest, IRequest<bool>
{
  public DeleteVoucherCommand(int voucherId)
  {
    VoucherId = voucherId;
  }

  public int VoucherId { get; }
}
