﻿using Domain.Cases.CreateVoucherCase;
using Domain.Entities.Postgres;
using Domain.Validators.CreateVoucher;
using Domain.ValueObjects;
using FluentValidation;
using FluentValidation.Results;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Application.CQRS.Write.CreateVoucher;

public sealed class CreateVoucherHandler : RequestHandler<CreateVoucherCommand, VoucherEntity>
{
  private readonly CreateVoucherCase _voucherCase;

  private readonly CurrencyValidator _currencyValidator;
  
  private readonly ILogger<CreateVoucherHandler> _logger;

  public CreateVoucherHandler(
    CreateVoucherCase voucherCase,
    CurrencyValidator currencyValidator, 
    ILogger<CreateVoucherHandler> logger
    )
  {
    _voucherCase = voucherCase;
    _currencyValidator = currencyValidator;
    _logger = logger;
  }
  protected override VoucherEntity Handle(CreateVoucherCommand command)
  {
    var createVoucherVo = new CreateVoucherVo(
      command.CreateVoucherDto.RequestCurrency,
      command.CreateVoucherDto.RequestAmount,
      command.CreateVoucherDto.RequestClientId,
      123 // terminalId is not part of the request, it should be passed from different layer
    );

    Validate(createVoucherVo);

    return _voucherCase.CreateVoucher(
      createVoucherVo
    );
  }

  private ValidationResult Validate(CreateVoucherVo createVoucherVo)
  {
    //Validators
    return _currencyValidator.Validate(createVoucherVo, options =>
    {
      options.ThrowOnFailures();
      options.IncludeRuleSets(CurrencyValidator.CurrencyValidationRules);
    });
  }
}
