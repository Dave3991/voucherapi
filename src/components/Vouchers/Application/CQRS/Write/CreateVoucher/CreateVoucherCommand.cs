﻿using Application.Dto;
using Domain.Entities.Postgres;
using MediatR;

namespace Application.CQRS.Write.CreateVoucher;

public class CreateVoucherCommand: IRequest<VoucherEntity>
{

  public CreateVoucherDto CreateVoucherDto { get; init; }
  
  public CreateVoucherCommand(CreateVoucherDto createVoucherDto)
  {
    CreateVoucherDto = createVoucherDto;
  }
}
