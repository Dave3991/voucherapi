﻿using Application.Interface;
using Domain.Entities.Postgres;
using MediatR;

namespace Application.CQRS.Read.GetVoucher;

public sealed class GetVoucherQueryHandler : RequestHandler<GetVoucherQuery, VoucherEntity>
{
  private readonly IGetVoucherRepository _getVoucherRepository;

  public GetVoucherQueryHandler(IGetVoucherRepository getVoucherRepository)
  {
    _getVoucherRepository = getVoucherRepository;
  }
  
  protected override VoucherEntity Handle(GetVoucherQuery request)
  {
    return _getVoucherRepository.GetVoucher(request.VoucherId);
  }
}
