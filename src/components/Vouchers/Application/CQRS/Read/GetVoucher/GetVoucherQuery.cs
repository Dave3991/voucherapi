﻿using Domain.Entities.Postgres;
using MediatR;

namespace Application.CQRS.Read.GetVoucher;

public class GetVoucherQuery: IRequest<VoucherEntity>
{
  public int VoucherId { get; init; }
  public GetVoucherQuery(int voucherId)
  {
    VoucherId = voucherId;
  }
}
