namespace Domain.Errors;

public class VoucherCreateError: Exception
{
  public VoucherCreateError(Exception e) : base(e.Message)
  {
    
  }
}
