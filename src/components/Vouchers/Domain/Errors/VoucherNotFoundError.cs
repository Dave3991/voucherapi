﻿namespace Domain.Errors;

public class VoucherNotFoundError: TypeLoadException
{
    public VoucherNotFoundError(string message) : base(message)
    {
    }
}
