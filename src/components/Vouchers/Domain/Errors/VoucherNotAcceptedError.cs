﻿namespace Domain.Errors;

public class VoucherNotAcceptedError: Exception
{
    public VoucherNotAcceptedError(string message) : base(message)
    {
    }
}
