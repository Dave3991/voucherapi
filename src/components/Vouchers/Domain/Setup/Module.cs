﻿using System.ComponentModel.Composition;
using Domain.Case;
using Domain.Cases.CreateVoucherCase;
using Domain.Validators.CreateVoucher;
using Domain.ValueObjects;
using Libs.Interface;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Domain.Setup;

// export so it can be discovered by MEF in ModuleConfigurator (see ModuleConfigurator.cs)
[Export(typeof(IModule))]
public class Module: IModule
{
  public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
  {
    services.AddTransient<CreateVoucherCase>();
    services.AddTransient<CreateVoucherVo>();
    services.AddTransient<DeleteVoucherCase>();
    services.AddTransient<CurrencyValidator>();
  }
}
