using Domain.ValueObjects;
using FluentValidation;

namespace Domain.Validators.CreateVoucher;

public class CurrencyValidator: AbstractValidator<CreateVoucherVo>
{
    public const string CurrencyValidationRules = "CurrencyValidationRules";
    public CurrencyValidator()
    {
      RuleSet(CurrencyValidationRules, () => 
      {
        RuleFor(x => x.Currency).NotEmpty().WithMessage("Currency is required");
      });
      
       
    }
}


