﻿namespace Domain.ValueObjects;

/// <summary>
/// A Value Object (VO) is an immutable object that represents a concept in the domain.
/// Its equality is based on its value, not its identity.
/// This is different from a Data Transfer Object (DTO), which is an object that carries data between processes.
/// A DTO doesn't contain any business logic and its equality is typically based on identity.
/// In this case, CreateVoucherVo is a VO that represents the concept of a voucher creation request in the domain.
/// </summary>
public record CreateVoucherVo
{
  public string Currency { get; }

  public decimal Amount { get; }

  public string ClientId { get; }

  public int TerminalId { get; }

  public CreateVoucherVo(string currency, decimal amount, string clientId, int terminalId)
  {
    Currency = currency;
    Amount = amount;
    ClientId = clientId;
    TerminalId = terminalId;
  }
}
