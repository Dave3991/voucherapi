using Domain.Entities.Postgres;
using MediatR;

namespace Domain.Events;

public class VoucherCreatedEvent : INotification
{
  public int VoucherId { get; }

  public VoucherCreatedEvent(VoucherEntity voucher)
  {
    VoucherId = voucher.Id;
  }
}
