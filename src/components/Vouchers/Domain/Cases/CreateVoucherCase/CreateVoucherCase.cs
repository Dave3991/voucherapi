using Domain.Entities.Postgres;
using Domain.Errors;
using Domain.Interfaces;
using Domain.ValueObjects;
using Microsoft.Extensions.Logging;

namespace Domain.Cases.CreateVoucherCase;

public class CreateVoucherCase
{
  private readonly ICreateVoucherRepository _repository;
  private readonly ILogger<CreateVoucherCase> _logger;

  public CreateVoucherCase(ICreateVoucherRepository repository, ILogger<CreateVoucherCase> logger)
  {
    _repository = repository;
    _logger = logger;
  }
  public VoucherEntity CreateVoucher(CreateVoucherVo createVoucherVo)
  {
    try
    {
      return _repository.CreateVoucher(createVoucherVo);
    }
    // handling only the business errors here,
    // infrastructure errors should be handled in the infrastructure layer
    catch (VoucherNotFoundError ex)
    {
      _logger.LogError(ex, "Voucher not found");
      throw new VoucherCreateError(ex);
    }
    catch (VoucherNotAcceptedError ex)
    {
      throw new VoucherCreateError(ex);
    }
    catch (Exception ex)
    {
      throw new VoucherCreateError(ex);
    }
  }
}
