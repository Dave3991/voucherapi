namespace Domain.Entities.Postgres;

public class VoucherEntity
{
  public int Id { get; set; }
  
  public decimal Discount { get; set; }
  public string VoucherCode { get; }
  
  public DateTimeOffset ExpirationDate { get; set; }
  
  public bool IsActive { get; set; }
  
  public VoucherEntity(string voucherCode)
  {
    VoucherCode = voucherCode;
  }
}
