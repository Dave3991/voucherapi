﻿using Domain.Entities.Postgres;
using Domain.ValueObjects;

namespace Domain.Interfaces;

public interface ICreateVoucherRepository
{
  public VoucherEntity CreateVoucher(CreateVoucherVo createVoucherVo);
}
