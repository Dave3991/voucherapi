﻿using System.ComponentModel.Composition;
using System.Reflection;
using Libs.Interface;
using MediatR;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using UI.Http.Controllers;

namespace UI.Setup;

// export so it can be discovered by MEF in ModuleConfigurator (see ModuleConfigurator.cs)
/// <inheritdoc />
[Export(typeof(IModule))]
public class Module: IModule
{
  /// <inheritdoc />
  public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
  {
    var assembly = typeof(IControllerVoucher).Assembly;
    services.AddMvcCore().AddApplicationPart(assembly).AddControllersAsServices();
  }
}
