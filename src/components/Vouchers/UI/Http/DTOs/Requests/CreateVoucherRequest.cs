using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace UI.Http.DTOs.Requests;

/// <summary>A CreateVoucher request</summary>
public record CreateVoucherRequest
{
  /// <summary>
  /// Voucher request data
  /// </summary>
  /// <param name="currency"></param>
  /// <param name="amount"></param>
  /// <param name="clientId"></param>
  public CreateVoucherRequest(string currency, decimal amount, string clientId)
  {
    Currency = currency;
    Amount = amount;
    ClientId = clientId;
  }

  /// <param>Set voucher currency
  ///   <name>Currency</name>
  /// </param>
  [DefaultValue("PLN")]
  public string Currency { get; init; } = "PLN";

  /// <summary>Amount of money on Voucher</summary>
  [Required]
  public decimal Amount { get; init; }
  
  /// <summary>
  /// Client id
  /// </summary>
  public string ClientId { get; init; }
}
