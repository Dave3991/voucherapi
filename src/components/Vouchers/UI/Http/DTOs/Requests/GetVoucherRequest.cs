using System.ComponentModel.DataAnnotations;

namespace UI.Http.DTOs.Requests
{
  /// <summary>
  /// GetVoucherRequest DTO class
  /// </summary>
  public record GetVoucherRequest()
  {
  
    /// <summary>
    /// Voucher ID used for lookup for the voucher
    /// </summary>
    [Required]
    public string VoucherId { get; init; }
  }
}
