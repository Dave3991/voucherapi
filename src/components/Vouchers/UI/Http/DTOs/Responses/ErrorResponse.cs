﻿namespace UI.Http.Dto.Response;

/// <summary>
/// Error response.
/// </summary>
/// <param name="Message"></param>
public record ErrorResponse(string Message);
