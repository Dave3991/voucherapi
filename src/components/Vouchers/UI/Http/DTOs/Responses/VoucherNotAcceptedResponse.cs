﻿namespace UI.Http.Dto.Response;

/// <summary>
/// VoucherNotAcceptedResponse 
/// </summary>
public class VoucherNotAcceptedResponse
{
  /// <summary>
  /// Message with details about the error
  /// </summary>
  public string Message { get; }

  /// <summary>
  /// VoucherNotAcceptedResponse constructor
  /// </summary>
  /// <param name="message"></param>
  public VoucherNotAcceptedResponse(string message)
  {
    Message = message;
  }
}
