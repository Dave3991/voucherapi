using UI.Http.Controllers;

namespace UI.Http.Dto.Response;

/// <summary>
/// Response DTO for the <see cref="GetVoucherController"/>.
/// </summary>
public record CreateVoucherResponse
{
  /// <summary>
  /// The voucher ID for the created voucher.
  /// </summary>
  public string VoucherId { get; init; }
  /// <summary>
  /// The voucher ID for the created voucher.
  /// </summary>
  /// <param name="voucherId"></param>
  public CreateVoucherResponse(string voucherId)
  {
    VoucherId = voucherId;
  }

}
