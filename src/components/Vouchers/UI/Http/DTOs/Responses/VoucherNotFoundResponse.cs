﻿namespace UI.Http.Dto.Response;

/// <summary>
/// VoucherNotFoundResponse
/// </summary>
public class VoucherNotFoundResponse
{
  /// <summary>
  /// Message of the response
  /// </summary>
  public string Message { get; set; }
  /// <summary>
  /// VoucherNotFoundResponse constructor
  /// </summary>
  /// <param name="message"></param>
  /// <exception cref="ArgumentNullException"></exception>
  public VoucherNotFoundResponse(string message)
  {
    Message = message ?? throw new ArgumentNullException(nameof(message));
  }

  
}
