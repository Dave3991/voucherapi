﻿using Application.CQRS.Write.CreateVoucher;
using Application.Dto;
using Domain.Errors;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UI.Http.Dto.Response;
using UI.Http.DTOs.Requests;

namespace UI.Http.Controllers;

/// <summary>
/// Voucher Controller for Http
/// </summary>
[Route("api/v1/voucher")]
[Produces("application/json")]
[ApiController]
public class CreateVoucherController: ControllerBase, IControllerVoucher
{
    private readonly IMediator _mediator;
    
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="mediator"></param>
    public CreateVoucherController(IMediator mediator)
    {
      _mediator = mediator;
    }
    /// <summary>
    /// Creates a new voucher
    /// </summary>
    /// <param name="request"></param>
    /// <returns>A newly created Voucher</returns>
    /// <response code="200">Returns the newly created Voucher</response>
    /// <remarks>
    ///
    ///     POST /voucher
    ///     {
    ///        "currency": PLN,
    ///        "amount": 100.56,
    ///        "clientId": "54564"
    ///     }
    ///
    /// </remarks>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CreateVoucherResponse))]
    [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(VoucherNotFoundResponse))]
    [ProducesResponseType(StatusCodes.Status406NotAcceptable, Type = typeof(VoucherNotAcceptedResponse))]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponse))]
    public IActionResult Post(CreateVoucherRequest request)
    {
       var createVoucherDto = new CreateVoucherDto(
         request.Currency,
         request.Amount,
         request.ClientId
         );
       try
       {
         var voucherEntity = _mediator.Send(new CreateVoucherCommand(createVoucherDto)).Result;
         return Ok(new CreateVoucherResponse(voucherEntity.VoucherCode));
       }
       catch (VoucherNotFoundError e)
       {
         return NotFound(new VoucherNotFoundResponse(e.Message));
       }
       catch (VoucherNotAcceptedError e)
       {
         return StatusCode(StatusCodes.Status406NotAcceptable, new VoucherNotAcceptedResponse(e.Message));
       }
       catch (ValidationException e)
       {
         return StatusCode(StatusCodes.Status406NotAcceptable, new VoucherNotAcceptedResponse(e.Message));
       }
       catch (Exception e)
       {
         return StatusCode(StatusCodes.Status500InternalServerError, new ErrorResponse(e.Message));
       }
    }
    
}
