﻿using Application.CQRS.Read.GetVoucher;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UI.Http.Dto.Response;

namespace UI.Http.Controllers;

/// <summary>
/// Voucher Controller for Http
/// </summary>
[Route("api/v1/voucher")]
[Produces("application/json")]
[ApiController]
public class GetVoucherController: ControllerBase, IControllerVoucher
{
    private readonly IMediator _mediator;

    /// <inheritdoc />
    public GetVoucherController(IMediator mediator)
    {
      _mediator = mediator;
    }
    
    // https://docs.microsoft.com/cs-cz/aspnet/core/web-api/action-return-types?view=aspnetcore-6.0

    /// <summary>
    /// Get a specific voucher.
    /// </summary>
    /// <param name="VoucherId"></param>
    /// <returns>A voucher</returns>
    [HttpGet("{VoucherId}", Name = "GetByVoucherId")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(VoucherNotFoundResponse))]
    public IActionResult Get(int VoucherId)
    {
      return Ok(_mediator.Send(new GetVoucherQuery(VoucherId)));
    }

}
