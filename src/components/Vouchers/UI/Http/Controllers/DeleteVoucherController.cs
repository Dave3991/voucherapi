﻿using Application.CQRS.Write.DeleteVoucher;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UI.Http.Dto.Response;

namespace UI.Http.Controllers;

/// <summary>
/// Voucher Controller for Http
/// </summary>
[Route("api/v1/voucher")]
[Produces("application/json")]
[ApiController]
public class DeletetVoucherController: ControllerBase, IControllerVoucher
{
    private readonly IMediator _mediator;

    /// <inheritdoc />
    public DeletetVoucherController(IMediator mediator)
    {
      _mediator = mediator;
    }
    
    // https://docs.microsoft.com/cs-cz/aspnet/core/web-api/action-return-types?view=aspnetcore-6.0

    /// <summary>
    /// Delete a specific voucher.
    /// </summary>
    /// <param name="VoucherId"></param>
    /// <returns>Success or error</returns>
    [HttpDelete("{VoucherId}", Name = "DeleteByVoucherId")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(VoucherNotFoundResponse))]
    [ProducesResponseType(StatusCodes.Status501NotImplemented)]
    public IActionResult Get(int VoucherId)
    {
      try
      {
        _mediator.Send(new DeleteVoucherCommand(VoucherId));
        return Ok(new { Message = "Voucher deleted" });
      }
      catch (Exception e)
      {
        return StatusCode(StatusCodes.Status500InternalServerError, new ErrorResponse(e.Message));
      }
     
    }

}
