MIN_MAKE_VERSION := 3.82

ifneq ($(MIN_MAKE_VERSION),$(firstword $(sort $(MAKE_VERSION) $(MIN_MAKE_VERSION))))
$(error GNU Make $(MIN_MAKE_VERSION) or higher required)
endif

.DEFAULT_GOAL:=help

##@ Development
.PHONY: up down
up: ## start development environment, not Application!
	docker-compose -f ./docker-compose-development.yml up
down: ## stop development environment
	docker-compose -f ./docker-compose-development.yml down -v

##@ Local dockerized development
.PHONY: up-local down-local
up-local: ## start development environment
	docker-compose -f ./docker-compose-local-dockerized-development.yml up
down-local: ## stop development environment
	docker-compose -f ./docker-compose-local-dockerized-development.yml down -v

##@ Test
.PHONY: test-unit
test-unit: ## run unit tests
	#todo: test-unit

##@ CI
.PHONY: ci
ci: ci-lint ci-build ci-push ## run CI pipeline locally

ci-lint: ## runs linters
	make lint
ci-build: ## build CI pipeline
	docker-compose -f ./application/Server/docker-compose-ci.yml build
ci-push: ## push app container to docker registry
	#todo: ci-push

##@ Linters
.PHONY: lint
lint: lint-shell lint-hadolint lint-yamllint ## run linters

lint-shell: ## run shell linter
	@.ci/lint/shellcheck.sh
lint-hadolint: ## run hadolint linter
	@.ci/lint/hadolint.sh
lint-yamllint: ## run yamllint linter
	@.ci/lint/yamllint.sh

##@ Security
.PHONY: security
security: ## run security checks
	@.ci/security/security-scan.sh

.PHONY: help
help:
	@awk 'BEGIN {FS = ":.*##"; printf "Usage: \033[36mmake \033[96m<target>\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-25s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)
