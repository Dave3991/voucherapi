using NSubstitute;
using Domain.Cases.CreateVoucherCase;
using Domain.Entities.Postgres;
using Domain.ValueObjects;
using Infrastructure.Postgres.Repository.CreateVoucherRepository;
using MediatR;
using Microsoft.Extensions.Logging;


namespace Domain;

public class Tests
{
  [Test]
  public void TestCreateVoucherCase()
  {
    var expectedVoucher = new VoucherEntity("afawfawf");
    
    var mediator = Substitute.For<IMediator>();
    var logger = Substitute.For<ILogger<CreateVoucherCase>>();
    var voucherRepository = new CreateVoucherRepository(mediator);
    
    var createVoucherCase = new CreateVoucherCase(voucherRepository, logger);
    var voucherVo = new CreateVoucherVo("PLN", 12.255M, "clintId", 1);
    var voucher = createVoucherCase.CreateVoucher(voucherVo);
    Assert.That(voucher.VoucherCode, Is.SameAs(expectedVoucher.VoucherCode));
  }
}
