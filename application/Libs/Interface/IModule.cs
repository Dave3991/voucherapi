﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Libs.Interface
{
  /// <summary>
  /// Interface for the configuration of the module.
  /// </summary>
  public interface IModule
  {
    /// <summary>
    /// Method to register services in the DI container
    /// </summary>
    /// <param name="services"></param>
    /// <param name="configuration"></param>
    void ConfigureServices(IServiceCollection services, IConfiguration configuration);
  }
}
