﻿using System;
using System.Diagnostics;
using System.Diagnostics.Metrics;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Formatting = System.Xml.Formatting;

namespace Server.Behavior.Mediatr;

public class LoggingBehaviorPipeline<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse> where TRequest : IRequest<TResponse>
{
    private readonly ILogger<LoggingBehaviorPipeline<TRequest, TResponse>> _logger;
    private readonly Stopwatch _stopwatch = new Stopwatch();

    public LoggingBehaviorPipeline(ILogger<LoggingBehaviorPipeline<TRequest, TResponse>> logger)
  {
    _logger = logger;
  }

  // https://lurumad.github.io/cross-cutting-concerns-in-asp-net-core-with-meaditr
  public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
  {
    // https://dev.to/raulismasiukas/logging-pipeline-with-mediatr-5d4f
    var requestNameWithGuid = $"{request.GetType().Name} [{Guid.NewGuid().ToString()}]";
    _logger.LogInformation("Handling {RequestType}", requestNameWithGuid);

    try
    {
      _stopwatch.Reset();
      _stopwatch.Start();
      var response = await next();
      _stopwatch.Stop();
      _logger.LogInformation("Handled {RequestType} in {Time}ms", requestNameWithGuid, _stopwatch.ElapsedMilliseconds);
      return response;
    }
    catch (Exception ex)
    {
      // log every request property and its value
      var requestProperties = request.GetType().GetProperties();
      // add information about the exception into exception's data
      
      foreach (var requestProperty in requestProperties)
      {
        // add information about the exception into exception's data
        ex.Data.Add($"{requestProperty.Name}", requestProperty.GetValue(request));
      }
      
      //log the exception with all the data added to it
      _logger.LogError(ex, "Error handling {RequestType} with {Data}", requestNameWithGuid, JsonConvert.SerializeObject(ex.Data));

      throw;
    }
  }
}
