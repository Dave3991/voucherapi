using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using MediatR;
using Server.Configurators.Module;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Server.Behavior.Mediatr;

namespace Server
{
  public class Program
  {
    public static void Main(string[] args)
    {
      var builder = CreateHostBuilder(args);
      var app = builder.Build();
      app.Run();
    }

    public static IHostBuilder CreateHostBuilder(string[] args) =>
      new HostBuilder()
        .ConfigureHostConfiguration(builder =>
        {
          /* Host configuration */
        })
        .ConfigureAppConfiguration((host, builder) =>
        {
          builder.SetBasePath(Directory.GetCurrentDirectory());
          builder.AddJsonFile($"appsettings.{host.HostingEnvironment.EnvironmentName}.json", optional: false,
            reloadOnChange: true);
          builder.AddEnvironmentVariables();
          builder.AddCommandLine(args);
        })
        .ConfigureServices((host,services) =>
        {
          services.AddMediatR(AppDomain.CurrentDomain.GetAssemblies());
          // services.AddMediatR(typeof(Program)); the new way to add mediatr
          services.AddTransient(typeof(IPipelineBehavior<,>), typeof(LoggingBehaviorPipeline<,>));
          services.RegisterModulesServices(host.Configuration);
          services.AddOptions();
        })
        .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); })
        .UseSerilog((hostBuilder, configuration) =>
        {
          configuration.ReadFrom.Configuration(hostBuilder.Configuration);
          configuration.WriteTo.Seq(hostBuilder.Configuration.GetSection("SeqServerUrl").Value);
        }, preserveStaticLogger: true);
  }
}
