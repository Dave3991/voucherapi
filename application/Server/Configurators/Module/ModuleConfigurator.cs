﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.ReflectionModel;
using System.Linq;
using System.Reflection;
using Libs.Interface;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Server.Configurators.Module
{
  public static class ModuleConfigurator
  {
    private static readonly CompositionContainer _compositionContainer = GetCompositionContainer();
    
    public static void RegisterModulesServices(this IServiceCollection services, IConfiguration configuration)
    {
      var modules = _compositionContainer.GetExportedValues<IModule>();
      foreach (var module in modules)
      {
        module.ConfigureServices(services, configuration);
      }
    }

    // https://makolyte.com/csharp-how-to-load-assemblies-at-runtime-using-microsoft-extensibility-framework-mef/
    private static CompositionContainer GetCompositionContainer()
    {
      var catalog = new AggregateCatalog();
      catalog.Catalogs.Add(new DirectoryCatalog(AppDomain.CurrentDomain.BaseDirectory));
      return new CompositionContainer(catalog);
    }
    
  }
}
