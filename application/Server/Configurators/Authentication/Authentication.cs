﻿using System.Configuration;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Server.Configurators.Authentication;

public class Authentication
{
  public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
  {
    var identityUrl = configuration.GetValue<string>("IdentityUrl");

    // Add Authentication services

    services.AddAuthentication(options =>
    {
      options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
      options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

    }).AddJwtBearer(options =>
    {
      options.Authority = identityUrl;
      options.RequireHttpsMetadata = false;
      options.Audience = "orders";
    });
  }
}
