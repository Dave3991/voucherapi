using Microsoft.Extensions.Primitives;

namespace SemanticKernel.Modules.Ainews.Application.Tracking;

public class TrackingMiddleware
{
	private readonly RequestDelegate _next;
	
	private readonly ILogger<TrackingMiddleware> _logger;

	public TrackingMiddleware(RequestDelegate next, ILogger<TrackingMiddleware> logger)
	{
		_next = next;
		_logger = logger;
	}

	public async Task InvokeAsync(HttpContext context)
	{
		// Generate UUID
		var requestId = GenerateUUID();

		using (_logger.BeginScope("RequestId: {RequestId}", requestId))
		{
			// Set the UUID in Elastic APM
			await SetUUIDToElasticAPM(requestId);

			// Add UUID to Response Headers for tracking purposes
			await AddUUIDToResponseHeaders(context, requestId);

			// Proceed with the pipeline
			await _next(context);
		}
	}
	
	private Guid GenerateUUID()
	{
		return Guid.NewGuid();
	}
	
	private Task SetUUIDToElasticAPM(Guid uuid)
	{
		var transaction = Elastic.Apm.Agent.Tracer.CurrentTransaction;
		if (transaction != null)
		{
			transaction.SetLabel("UUID", uuid.ToString());
		}
		return Task.CompletedTask;
	}
	
	private Task AddUUIDToResponseHeaders(HttpContext context, Guid uuid)
	{
		context.Response.OnStarting(() => {
			context.Response.Headers.Append("X-Request-ID", uuid.ToString());
			return Task.CompletedTask;
		});
		return Task.CompletedTask;
	}
}
