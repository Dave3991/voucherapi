

<a href="https://twitter.com/intent/follow?screen_name=warezak_">
    <img src="https://img.shields.io/twitter/follow/warezak_?style=social" alt="Follow @warezak_" />
</a>

# Architecture should guide us to decisions which are good or better
Good architecture acts as a roadmap, helping developers navigate complex requirements and technical challenges by providing frameworks and guidelines that lead to robust and sustainable software solutions


## Hexagonal Architecture in .NET with DDD
A starting point is in `/src/application/Server/Program.cs`


trying to show what I think is the best way to create a hexagonal architecture in .NET

### Project Structure
![project_structure](./docs/images/project_structure.png "Project structure")

## Server project
## Libs project

## src/components/Vouchers projects
### Application
#### CQRS
 - I split CQRS to read and write, to make developers think about the separation of concerns
#### DTOs
 - I use DTOs to transfer data between layers
#### Interfaces
 - sometimes (mostly read use cases) has no business logic, so I omit to create "case" in DOMAIN layer and handle it in the APPLICATION layer

### Domain
### Infrastructure
### UI

## Lessons learned
### 1. be careful with the name of the project assembly, they can't be the the same so you can't have two UIs in different projects
So I renamed Assembly name of the UI project to `Vouchers.UI` and it worked
![assembly_name](./docs/images/asssembly_name.jpg "renaming assembly")